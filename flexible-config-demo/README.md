Test with Docker:

    docker build -t flexible-config-example .
    # Production environment
    docker run --rm -it -e "ENV=prod" flexible-config-example
    # Development environment
    docker run --rm -it -e "ENV=dev" flexible-config-example

Test without docker:

    ENV="prod"\
    FC_ENABLE=1 \
    FC_OUT="./generated-config.json" \
    FC_PARTIALS="./config/partials" \
    FC_SETTINGS="./config/settings/$ENV" \
    FC_TEMPLATES="./config/templates" \
    krakend run -c krakend.tmpl
